package handler

import (
	"bitbucket.org/iminsoft/vlan-srv/db"
	"github.com/micro/go-micro/server"
	"golang.org/x/net/context"
)

type (
	Vlan struct {
	}
)

func (v *Vlan) Get(ctx context.Context, req *GetRequest, res *GetResponse) error {
	u, err := db.Get(req.Gid)
	if err != nil {
		return err
	}

	res.Available = u.VLanList
	res.Used = u.VLanUsed

	return nil
}

func (v *Vlan) Use(ctx context.Context, req *UseRequest, res *UseResponse) error {
	u, err := db.Get(req.Gid)
	if err != nil {
		return err
	}

	err = u.Use(req.Used)
	if err != nil {
		return err
	}

	err = db.Set(u)
	if err != nil {
		return err
	}

	return nil
}

func (v *Vlan) Define(ctx context.Context, req *DefineRequest, res *DefineResponse) error {
	u, err := db.Get(req.Gid)
	if err != nil {
		return err
	}

	u.List(req.List)

	err = db.Set(u)
	if err != nil {
		return err
	}

	return nil
}

func Register(s server.Server) {
	s.Handle(s.NewHandler(&Vlan{}))
}
