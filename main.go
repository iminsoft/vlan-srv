package main

import (
	"log"

	micro "github.com/micro/go-micro"

	_ "bitbucket.org/iminsoft/vlan-srv/db"
	"bitbucket.org/iminsoft/vlan-srv/handler"
	_ "bitbucket.org/iminsoft/vlan-srv/subscriber"
)

func main() {
	service := micro.NewService(
		micro.Name("im.srv.vlan"),
	)

	service.Init()

	handler.Register(service.Server())

	if err := service.Run(); err != nil {
		log.Fatal(err)
	}

}
