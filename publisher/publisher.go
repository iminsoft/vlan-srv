package publisher

import (
	"log"

	ad "bitbucket.org/iminsoft/ad-srv/model"
	vlan "bitbucket.org/iminsoft/vlan-srv/model"
	"github.com/micro/go-micro/client"
	ctx "golang.org/x/net/context"
)

var (
	cli client.Client
)

func init() {
	cli = client.NewClient(
		client.ContentType("application/json"),
	)
}

func ModifyVLan(u vlan.User) error {
	req := &ad.ModifyRequest{
		DN: u.Dn,
		Attributes: map[string][]string{
			"xradiustunnelprivategroupid": {u.VLanUsed},
		},
	}

	res := &ad.ModifyResponse{}
	if err := rpc("im.srv.ad", "Record.Modify", req, res); err != nil {
		return err
	}

	return nil
}

func rpc(srv, name string, req, res interface{}) error {
	err := cli.Call(ctx.Background(), cli.NewRequest(srv, name, req), res)
	if err != nil {
		log.Println(err.Error())
		return err
	}

	return nil
}
