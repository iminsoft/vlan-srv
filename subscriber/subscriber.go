package subscriber

import (
	"fmt"
	"log"

	"bitbucket.org/iminsoft/vlan-srv/db"
	. "bitbucket.org/iminsoft/vlan-srv/model"
	"github.com/micro/go-micro/broker"
)

var (
	listeners = map[string]func([]byte) error{
		"ad.record": adRecord,
	}
)

func init() {

	if err := broker.Init(); err != nil {
		log.Fatalf("Broker Init error: %v", err)
	}

	if err := broker.Connect(); err != nil {
		log.Fatalf("Broker Connect error: %v", err)
	}

	for e, fn := range listeners {
		log.Printf("Listening event: %s\n", e)
		go listen(e, fn)
	}
}

func adRecord(b []byte) error {

	e, err := buildElement(b)
	if err != nil {
		return err
	}

	u := User{
		Dn:       e.dn(),
		Gid:      e.gid(),
		VLanUsed: e.vLanUsed(),
		VLanList: e.vLanList(),
	}

	return db.SetNotExists(u)
}

func listen(topic string, f func([]byte) error) {
	_, err := broker.Subscribe(topic, func(p broker.Publication) error {
		return f(p.Message().Body)
	})

	if err != nil {
		fmt.Println(err)
	}
}
