package model

import "fmt"

type (
	User struct {
		Gid      string   `json:"gid"`
		Dn       string   `json:"dn"`
		VLanUsed string   `json:"v_lan_used"`
		VLanList []string `json:"v_lan_list"`
	}
)

func (u *User) Use(vlan string) error {
	if vlan != "" {
		has := false
		for _, v := range u.VLanList {
			if v == vlan {
				has = true
				break
			}
		}

		if !has {
			return fmt.Errorf("Vlan %s can not be used, not exist in definition", vlan)
		}
	}

	u.VLanUsed = vlan

	return nil
}

func (u *User) List(vlans []string) {
	u.VLanList = vlans
	u.Use("")

}
