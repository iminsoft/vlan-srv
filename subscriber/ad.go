package subscriber

import (
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
)

type adElement map[string][]string

func buildElement(b []byte) (adElement, error) {
	e := &adElement{}

	return *e, json.Unmarshal(b, e)
}

func (a adElement) get(name string) string {

	if e, is := a[name]; is {
		return e[0]
	}

	return ""
}

func (a adElement) dn() string {
	return a.get("distinguishedName")
}

func (a adElement) gid() string {
	g := a.get("objectGUID")
	od, _ := base64.StdEncoding.DecodeString(g)

	return hex.EncodeToString(od)
}

func (a adElement) vLanUsed() string {
	return a.get("xradiustunnelprivategroupid")
}

func (a adElement) vLanList() []string {
	return []string{a.vLanUsed()}
}
