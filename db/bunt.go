package db

import (
	"encoding/json"

	"fmt"

	. "bitbucket.org/iminsoft/vlan-srv/model"
	"bitbucket.org/iminsoft/vlan-srv/publisher"
	"github.com/tidwall/buntdb"
)

var (
	name = "vlan.db"
	db   *buntdb.DB
)

func init() {
	var err error
	db, err = buntdb.Open(name)
	if err != nil {
		panic(err.Error())
	}
}

func SetNotExists(u User) error {
	return db.Update(func(tx *buntdb.Tx) error {

		if u.Gid == "" {
			return fmt.Errorf("Identifier of User is not defined")
		}

		_, err := tx.Get(u.Gid)
		if err != nil {
			_, _, err = tx.Set(u.Gid, jString(u), nil)

			return err
		}

		return nil
	})
}

func Set(u User) error {
	return db.Update(func(tx *buntdb.Tx) error {
		if u.Gid == "" {
			return fmt.Errorf("Identifier of User is not defined")
		}

		tx.Delete(u.Gid)

		err := publisher.ModifyVLan(u)
		if err != nil {
			return err
		}
		_, _, err = tx.Set(u.Gid, jString(u), nil)

		return err
	})
}

func Get(gid string) (User, error) {
	u := User{}
	return u, db.View(func(tx *buntdb.Tx) error {
		v, err := tx.Get(gid)
		if err != nil {
			return err
		}

		return bStruct(v, &u)
	})
}

func jString(i interface{}) string {
	j, err := json.Marshal(i)
	if err != nil {
		return ""
	}

	return string(j)
}

func bStruct(j string, i interface{}) error {
	return json.Unmarshal([]byte(j), i)
}
