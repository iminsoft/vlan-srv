package handler

type (
	GetRequest struct {
		Gid string `json:"gid"`
	}

	GetResponse struct {
		Used      string   `json:"used"`
		Available []string `json:"available"`
	}

	UseRequest struct {
		Gid  string `json:"gid"`
		Used string `json:"used"`
	}

	UseResponse struct {
	}

	DefineRequest struct {
		Gid  string   `json:"gid"`
		List []string `json:"list"`
	}

	DefineResponse struct {
	}
)
